<?php
/**
 * @author Nikola Kostadinov<nikolakk@gmail.com>
 * Date: 21.12.2015
 * Time: 13:52 �.
 */

namespace insight\registry\traits;

use insight\registry\models\forms\RegistryForm;
use Yii;

/**
 * Common functionalities to all Insight components.
 */
trait Settings
{
    /**
     * @var array The settings for the whole application.
     */
    public $settings = [];

    /**
     * Returns a setting's value by the setting's id.
     *
     * @param string $key The key of the setting.
     * @param integer $userId A user's id. By default is NULL.
     * @return string|bool The setting if found, false if not.
     */
    public function getSetting($key, $userId = null)
    {
        $value = Yii::$app->registry->get($key, $userId);
        if (!isset($value) && isset($this->settings[$key])) {
            if (is_array($this->settings[$key])) {
                return $this->settings[$key]['value'];
            }
            return $this->settings[$key];
        }
        return $value;
    }

    public function getSettingsForm()
    {
        $this->loadDefaultSettings();
        return Yii::createObject([
            'class' => RegistryForm::className(),
            'settings' => $this->settings,
        ]);
    }

    public function loadDefaultSettings()
    {
    }
}
