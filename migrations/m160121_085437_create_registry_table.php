<?php

use insight\registry\models\Registry;
use yii\db\Migration;
use yii\db\Schema;

class m160121_085437_create_registry_table extends Migration
{
    public function up()
    {
        $this->createTable(Registry::tableName(), [
            'id' => Schema::TYPE_PK,
            'key' => Schema::TYPE_STRING . '(45) NOT NULL',
            'value' => Schema::TYPE_STRING . '(60) NOT NULL',
        ]);
    }

    public function down()
    {
        $this->dropTable(Registry::tableName());
    }
}
