<?php

use insight\registry\models\Registry;
use yii\db\Migration;

class m160928_131603_expand_value_column_length extends Migration
{
    public function up()
    {
        $this->alterColumn(Registry::tableName(), 'value', $this->string(500)->notNull());
    }

    public function down()
    {
        $this->alterColumn(Registry::tableName(), 'value', $this->string(60)->notNull());
    }
}
