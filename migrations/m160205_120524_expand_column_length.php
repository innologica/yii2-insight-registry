<?php

use insight\registry\models\Registry;
use yii\db\Migration;

class m160205_120524_expand_column_length extends Migration
{
    public function up()
    {
        $this->alterColumn(Registry::tableName(), 'key', $this->string(100)->notNull());
    }

    public function down()
    {
        $this->alterColumn(Registry::tableName(), 'key', $this->string(45)->notNull());
    }
}
