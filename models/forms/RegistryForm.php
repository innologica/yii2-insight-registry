<?php

namespace insight\registry\models\forms;

use Yii;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\helpers\Inflector;
use yii\web\ForbiddenHttpException;

/**
 * A base form for editing registries.
 *
 * In order to edit settings, add the settings that you want to edit to the $settings array.
 * The form will authomatically generate everything that you need - will fetch the values, will add rules, will change attribute labels.
 * To save the settings, just call the save() method.
 *
 * All fields of the form are required.
 */
class RegistryForm extends Model
{
    /**
     * The array of settings that will be edited by the form. It supports two types of formats:
     *  - key-array format - The key is the name of the setting; the array contains configuration about this setting.
     *    The possible values of the array are:
     *      - value - Mandatory! The value of the setting;
     *      - isGlobal - Optional. If the setting is global only; true by default;
     *      - permission - Optional. If the setting needs a certain permission; false by default;
     *  - key-value format - In this case the array is converted into key-array format, where
     *    the missing fields isGlobal and permission accept their defaults.
     * @var array
     */
    public $settings = [];

    private $_dirtySettings = [];

    /**
     * Overwrites the $settings array with previously cached settings.
     */
    public function init()
    {
        parent::init();
        $this->addSettings($this->settings);
    }
    
    public function rules()
    {
        $rules = [];
        foreach ($this->settings as $key => $value) {
            $rules[] = [$key, 'default', 'value' => $value['value']];
        }
        return $rules;
    }

    /**
     * If the settings' keys have dots in their names, the method fetches the last
     * word after the last dot and uses it as an attribute's label.
     *
     * For example if the setting's key is 'accounting.currency.format',
     * the label of that setting will be 'Format'.
     * 
     * @return array
     */
    public function attributeLabels()
    {
        $attributeLabels = [];
        foreach (array_keys($this->settings) as $attributeName)
            if (strpos($attributeName, '.') != false) {
                $attributeNameSuffix = array_pop(explode('.', $attributeName));
                $attributeLabels[$attributeName] = Inflector::camel2words($attributeNameSuffix, true);
            }
        return $attributeLabels;
    }

    /**
     * Validates the form, and updates the cache only for the settings that are edited.
     *
     * @return boolean True on success, false if validation fails or the cache update fails.
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $success = 0;
        foreach ($this->_dirtySettings as $dirtyPropertyKey => $dirtyPropertyValue) {
            if (is_array($dirtyPropertyValue)) {
                $dirtyPropertyValue = implode(',', $dirtyPropertyValue);
            }
            
            $this->settings[$dirtyPropertyKey]['value'] = $dirtyPropertyValue;
            if ($this->settings[$dirtyPropertyKey]['isGlobal']) {
                $success += Yii::$app->registry->set($dirtyPropertyKey, $dirtyPropertyValue) ? 1 : 0;
            } else {
                $success += Yii::$app->registry->set($dirtyPropertyKey, $dirtyPropertyValue, Yii::$app->user->id) ? 1 : 0;
            }
        }
        
        return count($this->_dirtySettings) == $success;
    }

    public function __get($name)
    {
        if (isset($this->_dirtySettings[$name]))
            return $this->_dirtySettings[$name];
        if (isset($this->settings[$name]))
            return $this->settings[$name]['value'];
        return parent::__get($name);
    }

    public function __set($name, $value)
    {
        if (isset($this->settings[$name])) {
            if ($this->settings[$name]['value'] != $value) {
                if (($this->settings[$name]['permission'] && !Yii::$app->user->can($this->settings[$name]['permission'])))
                    throw new ForbiddenHttpException("You don't have permissions to set the '$name' setting!");
                else
                    $this->_dirtySettings[$name] = $value;
            }
        } else
            parent::__set($name, $value);
    }

    public function addSettings($settings)
    {
        foreach ($settings as $key => $value) {
            if (is_array($value)) {
                if (!isset($value['value'])) {
                    throw new InvalidConfigException('Missing key "value"');
                }
                $this->settings[$key] = ['value' => $value['value']];
            } else {
                $this->settings[$key] = ['value' => $value];
            }

            $this->settings[$key]['isGlobal'] = isset($value['isGlobal']) ? $value['isGlobal'] : true;
            $this->settings[$key]['permission'] = isset($value['permission']) ? $value['permission'] : false;
            
            if ($this->settings[$key]['isGlobal']) {
                $cachedValue = Yii::$app->registry->get($key);
            } else {
                $cachedValue = Yii::$app->registry->get($key, Yii::$app->user->id);
            }

            if (isset($cachedValue)) {
                $this->settings[$key]['value'] = $cachedValue;
            }
        }
    }
}
