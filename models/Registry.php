<?php

namespace insight\registry\models;

use yii\db\ActiveRecord;

/**
 * The model behind the registry table.
 */
class Registry extends ActiveRecord
{
    public static function tableName()
    {
        return 'registry';
    }
    
    public function rules()
    {
        return [
            [['key', 'value'], 'required'],
            ['key', 'string', 'max' => 100],
            ['value', 'string', 'max' => 500],
            ['user_id', 'integer'],
        ];
    }

    /**
     * Creates a Registry object (setting) by key and value.
     *
     * @param string $key The setting's key.
     * @param string $value The setting's value.
     * @return Registry The registry object
     */
    public static function create($key, $value)
    {
        $registry = new Registry();
        $registry->key = $key;
        $registry->value = $value;
        return $registry;
    }
}
