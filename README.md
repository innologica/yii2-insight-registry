Registry
========
A Yii2 plug-in for storing hierarchical low level settings.

Installation
------------

```
php composer.phar require --prefer-dist innologica/yii2-insight-registry "*"
```

or add

```
"innologica/yii2-insight-registry": "*"
```

to the require section of your `composer.json` file.

Configuration
-------------

Run migrations:

```
$ ./yii migrate --migrationPath=vendor/innologica/yii2-insight-registry/migrations
```

Add the registry component to your components' section:

```php
'components' => [
    'registry' => [
        'class' => '\insight\registry\components\Registry',
    ],
    'cache' => [
        'class' => 'yii\caching\FileCache', // Or something else
    ],
    ...
],
```

**Note:** The registry plugin uses cache. Don't forget to add and adjust a cache component as well!

Usage
-----

To set a setting:

```php
<?= Yii::$app->registry->set('settingKey', 'settingValue'); ?>
```

To set a setting for a specific user:

```php
<?= Yii::$app->registry->get('settingKey', 'settingValue', 123); ?>
```
To get a setting:

```php
<?= Yii::$app->registry->get('settingKey'); ?>
```

To get a setting for a specific user:

```php
<?= Yii::$app->registry->get('settingKey', 123); ?>
```

Settings management
-------------------

The most common way of adding/updating settings is through the RegistryForm.
To create the form you must pass the settings that you want to edit like that:

```php
$model = Yii::createObject([
    'class' => \insight\registry\models\forms\RegistryForm::className(),
    'settings' => [
        'settingKey.subkey1' => 'value 1',
        ...
    ],
]);
```

The form communicates with the registry component and will overwrite those RegistryForm::$settings that are found in the cache.

In the view:

```php
<?= $form->field($model, 'settingKey.subkey1')->textInput(); ?>
```

The label of the afore mentioned field will be `Subkey1`.

To remove all settings from the registry:

```php
Yii::$app->registry->clear();
```
**NOTE!!!** The settings will be deleted from the cache as well as their mirror in the database!
