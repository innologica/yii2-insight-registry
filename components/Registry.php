<?php
namespace insight\registry\components;

use insight\registry\models\Registry as RegistryModel;
use Yii;
use yii\base\Component;

/**
 * A central point for storing and fetching settings.
 */
class Registry extends Component
{
    /** @var string The cache key prefix. */
    public $cacheKeyPrefix = 'innologica.insight.registry';

    /** @var \yii\caching\Cache The caching object. */
    private $cache;

    public function init()
    {
        parent::init();
        $this->cache = Yii::$app->cache;
    }

    /**
     * Fetches a setting by its id.
     *
     * If the setting is not found in the cache, it is searched in the database.
     * If not found in the database as well, false is returned.
     *
     * If $userId param is set, the method searches for the setting of a specific user.
     *
     * @param string $key The key of the setting.
     * @param integer $userId A user's id if set. Null by default.
     * @return string|false The setting if found, false otherwise.
     */
    public function get($key, $userId = null)
    {
        $cacheKey = $this->generateCacheKey($key, $userId);

        // First check in cache
        $value = $this->cache->get($cacheKey);
        // If found, then return it
        if (isset($value) && $value !== false) {
            return $value;
        }

        // If not found, then try to fetch it from the database
        $registry = RegistryModel::findOne(['key' => $cacheKey]);
        // If found, then put it in cache and return it
        if ($registry) {
            $this->set($key, $registry->value, $userId);
            return $registry->value;
        }
        
        // Else return null
        return null;
    }

    /**
     * Sets a setting's value.
     *
     * If the $userId param is specified, the setting is stored for a specific user.
     *
     * @param string $key The key of the setting.
     * @param string $value The value of the setting.
     * @param integer $userId The id of the user if set. Null by default.
     * @return boolean True if successfuly set, false if not.
     */
    public function set($key, $value, $userId = null)
    {
        $cacheKey = $this->generateCacheKey($key, $userId);

        // Try to find it in the database
        $registry = RegistryModel::findOne(['key' => $cacheKey]);
        // If found, overwrite it
        if ($registry) {
            $registry->value = $value;
        } else {
            // If not found, create it
            $registry = RegistryModel::create($cacheKey, $value);
        }

        // Save it to the database
        if ($registry->save(false)) {
            // Put it in cache for later use
            return $this->cache->set($cacheKey, $value);
        }
        
        return false;
    }

    /**
     * Removes all settigns previously added by the component.
     *
     * NOTE!!! The settings will be deleted from the cache as well as their mirror in the database!
     * 
     * @return boolean whether the flush operation was successful.
     */
    public function clear()
    {
        $settings = RegistryModel::find()->all();
        $success = 0;
        foreach ($settings as $setting) {
            $success += Yii::$app->cache->delete($setting->key) ? 1 : 0;
        }
        
        return $success == count($settings) && RegistryModel::deleteAll();
    }

    /**
     * Creates a unique combination between the cache key prefix and the key itself.
     * If a user id is specified, the cache key will be unique for the given user.
     *
     * @param string $key The key of the setting.
     * @param integer $userId A user id if specified. Defaults to null.
     * @return string The cache key.
     */
    private function generateCacheKey($key, $userId = null)
    {
        $cacheKey = "$this->cacheKeyPrefix.$key";
        if ($userId) {
            $cacheKey .= ".$userId";
        }

        return $cacheKey;
    }
}
